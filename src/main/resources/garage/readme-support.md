# Support package
These are components required for the Application and used by Adapters.
These are domain agnostic.
These are external dependency jar candidates.

E.g.

    support.mail.PropertiesConfig
    support.properties.PropertiesReader