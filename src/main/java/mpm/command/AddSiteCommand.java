package mpm.command;

import mpm.fs.FsManipulator;
import mpm.xml.XmlManipulator;

public class AddSiteCommand extends CommandRun {
    @Override
    public String getName() {
        return "add-site";
    }

    @Override
    public String getLineDescription() {
        return "Adds site folders to project.";
    }

    @Override
    public void runBody() {
      new XmlManipulator().addReportPlugin();
      FsManipulator.addSite();
    }
}
