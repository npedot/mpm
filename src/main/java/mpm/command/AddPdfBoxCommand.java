package mpm.command;

import mpm.xml.XmlManipulator;

public class AddPdfBoxCommand extends CommandRun {
    @Override
    public String getName() {
        return "add-pdfbox";
    }

    @Override
    public String getLineDescription() {
        return "Adds PdfBox dependency.";
    }

    @Override
    public void runBody() {
        new XmlManipulator().addPdfBoxDependency();
    }
}
