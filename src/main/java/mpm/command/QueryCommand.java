package mpm.command;

import mpm.xml.XmlQuery;

public class QueryCommand extends CommandRun {
    @Override
    public String getName() {
        return "query";
    }

    @Override
    public String getLineDescription() {
        return "Query POM nodes.";
    }

    @Override
    public void runBody() {
        setResult(new XmlQuery().queryElement(getArgTokens()));

    }
}
